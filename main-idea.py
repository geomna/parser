from selenium import webdriver
import time

# путь к драйверу chrome
from selenium.common.exceptions import NoSuchElementException

chromedriver = 'C:\\Users\\User\\PycharmProjects\\Scrapping\\venv\\Lib\\site-packages\\chromedriver.exe'
options = webdriver.ChromeOptions()
# options.add_argument('headless')  # для открытия headless-браузера
driver = webdriver.Chrome(executable_path=chromedriver, options=options)

# get list of products
def cat_parse(category, url, back_url, level):
    driver.get(url)
    time.sleep(1)
    # search_field = driver.find_elements_by_css_selector("a.index-link")
    # driver.find_element_by_css_selector("button.show-as.list").click()
    sublevels = driver.find_elements_by_partial_link_text("Prikaži sve")
    if sublevels.__len__() > 0:
        sublevel_url_list = []
        for item in sublevels:
            sublevel_url_list.append(item.get_property("href"))

        level += 1
        print("________", category, "___________")

        for sub_url in sublevel_url_list:
            # tmp_url = cat_parse(category, sub_category.get_property("href"), url, level)
            cat_parse(category, sub_url, url, level)
            driver.get(url)
    else:
        list_category = driver.find_element_by_tag_name("h1[ng-bind='::category.name']")
        list_products = driver.find_elements_by_xpath("//div[@im-wsc-product='product']")
        cnt = 0
        for pp in list_products:
            image = pp.find_element_by_tag_name("img")
            price = pp.find_element_by_tag_name("p[class='cijena']")
            print(cnt, ";" * level, list_category.text, ";", image.get_property("alt"), ";", image.get_property("src"), ";", price.text)
            cnt += 1
    return back_url


# get list of categories
base_url = "https://www.ideaonline.me/#!/"
driver.get(base_url)
search_field = driver.find_elements_by_css_selector("a.index-link")
count = 0
category = dict()
for ii in search_field:
    category.update({ii.get_property("text"):ii.get_property("href")})
    print(count, ";", ii.get_property("text"), ";", ii.get_property("href"))
    count += 1

for name, href in category.items():
    back_url = cat_parse(name, href, base_url, 1)

    time.sleep(3)


driver.quit()
