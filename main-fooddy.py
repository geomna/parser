import json
import time

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

# путь к драйверу chrome


chromedriver = 'C:\\Users\\User\\PycharmProjects\\Scrapping\\venv\\Lib\\site-packages\\chromedriver.exe'
options = webdriver.ChromeOptions()
options.add_argument('--window-size=%s,%s' % (1900, 1200))

# options.add_argument('headless')  # для открытия headless-браузера
driver = webdriver.Chrome(executable_path=chromedriver, options=options)


# get list of products
def cat_parse(url):
    driver.get(url)
    # time.sleep(1)
    height = driver.execute_script("return document.body.scrollHeight")
    step = height // 1000

    for n in range(step):
        driver.execute_script("window.scrollBy(0 , 1000 );")
        time.sleep(1)

    if len(driver.find_elements_by_class_name('t-store__load-more-btn')) > 0:
        element = driver.find_elements_by_class_name('t-store__load-more-btn')[0].get_attribute("style")
        if 'display: none' not in element:
            driver.execute_script("window.scrollBy(0 , -700 );")
            button = driver.find_elements_by_class_name('t-store__load-more-btn')[0]
            # action.move_to_element(button).perform()  # hover and click into input field
            time.sleep(1)
            button.click()  # hover and click into input field
            time.sleep(2)
            height = driver.execute_script("return document.body.scrollHeight")
            position = button.location['y']
            step = (height - position)//500
            for n in range(step + 1):
                driver.execute_script("window.scrollBy(0 , 500 );")
                time.sleep(1)
        time.sleep(1)

    cat_name = driver.find_element_by_xpath("/ html / body / div[1] / div[2] / div / div / div[3] / div")
    items_list = driver.find_elements_by_class_name('t-store__card__wrap_all')

    payload = {}
    print('-----------------------')
    print(cat_name.text)
    payload[cat_name.text] = {}
    count = 0
    for item in items_list:
        try:
            count += 1
            parsed = {}
            parsed['item_name'] = item.find_element_by_xpath(".//div[contains(@class, 'js-product-name')]").text
            parsed['item_sku'] = item.find_element_by_xpath(".//div[contains(@class, 't-store__card__sku')]").text
            parsed['item_price'] = item.find_element_by_xpath(".//div[contains(@class, 't-store__card__price-item')]").text
            parsed['img'] = item.find_element_by_xpath('.//img').get_attribute('src')
            payload[cat_name.text][parsed['item_name']] = {'sku': parsed['item_sku'], 'price': parsed['item_price'], 'img': parsed['img']}
        except:
            pass
    print("Items parsed: " + str(count))
    return payload


# get list of categories
base_url = "https://shop.fooddy.me/russian_food/"
driver.get(base_url)
action = ActionChains(driver)

time.sleep(2)
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

burger = driver.find_elements_by_class_name('t830__menu__content')
action.move_to_element(burger[0]).click().perform()  # hover and click into input field
time.sleep(2)

fddy = {}
fddy_parsed = {}
categories = driver.find_elements_by_class_name('t830m__list-item')
for el in categories:
    print("\n" + el.text)
    fddy[el.text] = {}
    data = []
    try:
        div1 = el.find_element_by_xpath('./div[1]/div')
    except:
        div1 = el.find_element_by_xpath('./div[1]/a')
        print(div1.get_attribute('href'))
        data.append(div1.get_attribute('href'))
    try:
        div2 = el.find_elements_by_xpath('./div[2]//div/a')
        if len(div2) > 0:
            for a in div2:
                print(a.get_attribute('href'))
                data.append(a.get_attribute('href'))
    except:
        pass
    fddy[el.text]['data'] = data

for cat in fddy:
    fddy_parsed[cat] = {}
    for href in fddy[cat]['data']:
        parsed = cat_parse(href)
        key = list(parsed.keys())
        value = list(parsed.values())
        fddy_parsed[cat][key[0]] = value[0]

result = json.dumps(fddy_parsed, ensure_ascii=False, indent=2).encode('utf-8')

with open('mix-markt.txt', 'w', encoding='utf-8') as f:
    for key in list(fddy_parsed.keys()):
        for sub in list(fddy_parsed[key].keys()):
            for el in (fddy_parsed[key][sub].keys()):
                output_str = key + ' : ' + sub + ';' + el
                for n in list(fddy_parsed[key][sub][el].keys()):
                    if 'sku' in n:
                        if ":" in fddy_parsed[key][sub][el][n]:
                            output_str += ';' + fddy_parsed[key][sub][el][n].split(':')[1]
                        else:
                            output_str += ';' + fddy_parsed[key][sub][el][n]
                    else:
                        output_str += ';' + fddy_parsed[key][sub][el][n]
                print(output_str)
                print(output_str, file=f)

#
# with open('data.txt', 'w', encoding='utf8') as json_file:
#     json.dump(fddy_parsed, json_file, ensure_ascii=False)
#
#


driver.quit()
